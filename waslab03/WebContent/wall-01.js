var baseURI = "http://localhost:8080/waslab03";
var tweetsURI = baseURI + "/tweets";

var req;
var tweetBlock = "	<div id='tweet_{0}' class='wallitem'>\n\
	<div class='likes'>\n\
	<span class='numlikes'>{1}</span><br /> <span\n\
	class='plt'>people like this</span><br /> <br />\n\
	<button onclick='{5}Handler(\"{0}\")'>{5}</button>\n\
	<br />\n\
	</div>\n\
	<div class='item'>\n\
	<h4>\n\
	<em>{2}</em> on {4}\n\
	</h4>\n\
	<p>{3}</p>\n\
	</div>\n\
	</div>\n";

String.prototype.format = function () {
  var args = arguments;
  return this.replace(/{(\d+)}/g, function (match, number) {
    return typeof args[number] != 'undefined'
      ? args[number]
      : match
      ;
  });
};

function likeHandler(tweetID) {
  var target = 'tweet_' + tweetID;
  var uri = tweetsURI + "/" + tweetID + "/likes";
  // e.g. to like tweet #6 we call http://localhost:8080/waslab03/tweets/6/like

  req = new XMLHttpRequest();
  req.open('POST', uri, /*async*/true);
  req.onreadystatechange = function () {
    if (req.readyState == 4 && req.status == 200) {
      document.getElementById(target).getElementsByClassName("numlikes")[0].innerHTML = req.responseText;
    }
  };
  req.send(/*no params*/null);
}

function deleteHandler(tweetID) {
  var target = 'tweet_' + tweetID;
  var key = "tweet" + tweetID;
  var uri = tweetsURI + "/" + tweetID;
  // e.g. to delete tweet #6 we call http://localhost:8080/waslab03/tweets/6

  req = new XMLHttpRequest();
  req.open('DELETE', uri, /*async*/true);
  req.onreadystatechange = function () {
    if (req.readyState === 4 && req.status === 200) {
      const element = document.getElementById(target);
      element.parentNode.removeChild(element);
      localStorage.removeItem(key);
    }
  };
  var token = localStorage.getItem(key);
  req.send(JSON.stringify({[key]: token}));
}

function getTweetHTML(tweet, action) {  // action :== "like" xor "delete"
  var dat = new Date(tweet.date);
  var dd = dat.toDateString() + " @ " + dat.toLocaleTimeString();
  return tweetBlock.format(tweet.id, tweet.likes, tweet.author, tweet.text, dd, action);

}

function getTweets() {
  req = new XMLHttpRequest();
  req.open("GET", tweetsURI, true);
  req.onreadystatechange = function () {
    if (req.readyState === 4 && req.status === 200) {
      var tweet_list = JSON.parse(req.responseText);
      
      const tweetHtmlList = tweet_list.map(t => 
      localStorage.getItem("tweet" + t.id) === null ? getTweetHTML(t, 'like') : getTweetHTML(t, 'delete'));
      document.getElementById("tweet_list").innerHTML = tweetHtmlList;
    }
    ;
  };
  req.send(null);
};


function tweetHandler() {
  var author = document.getElementById("tweet_author").value;
  var text = document.getElementById("tweet_text").value;


  var uri = tweetsURI;

  req = new XMLHttpRequest();
  req.open('POST', uri, /*async*/true);
  req.onreadystatechange = function () {
    if (req.readyState === 4 && req.status === 200) {
      const {id, token} = JSON.parse(req.responseText);
      const tweet = {
        date: new Date(),
        text,
        id,
        likes: 0,
        author
      }
      let element = document.getElementById('tweet_list');
      element.innerHTML = getTweetHTML(tweet, 'delete') + element.innerHTML;
      localStorage.setItem('tweet' + id, token);
    }
  };
  req.send(JSON.stringify({author, text}));

  // clear form fields
  document.getElementById("tweet_author").value = "";
  document.getElementById("tweet_text").value = "";

};


//main
function main() {
  document.getElementById("tweet_submit").onclick = tweetHandler;
  getTweets();
};
