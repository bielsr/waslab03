package wallOfTweets;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.repackaged.org.json.JSONArray;
import com.google.appengine.repackaged.org.json.JSONException;
import com.google.appengine.repackaged.org.json.JSONObject;



@SuppressWarnings("serial")
@WebServlet(urlPatterns = {"/tweets", "/tweets/*"})
public class WallServlet extends HttpServlet {

	private String TWEETS_URI = "/waslab03/tweets/";

	@Override
	// Implements GET http://localhost:8080/waslab03/tweets
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setContentType("application/json");
		resp.setHeader("Cache-control", "no-cache");
		List<Tweet> tweets= Database.getTweets();
		JSONArray job = new JSONArray();
		for (Tweet t: tweets) {
			JSONObject jt = new JSONObject(t);
			jt.remove("class");
			job.put(jt);
		}
		resp.getWriter().println(job.toString());
	}

	@Override
	// Implements POST http://localhost:8080/waslab03/tweets/:id/likes
	//        and POST http://localhost:8080/waslab03/tweets
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String uri = req.getRequestURI();
		int lastIndex = uri.lastIndexOf("/likes");
		if (lastIndex > -1) {  // uri ends with "/likes"
			// Implements POST http://localhost:8080/waslab03/tweets/:id/likes
			long id = Long.valueOf(uri.substring(TWEETS_URI.length(),lastIndex));		
			resp.setContentType("text/plain");
			resp.getWriter().println(Database.likeTweet(id));
		}
		else { 
			// Implements POST http://localhost:8080/waslab03/tweets
			int max_length_of_data = req.getContentLength();
			byte[] httpInData = new byte[max_length_of_data];
			ServletInputStream  httpIn  = req.getInputStream();
			httpIn.readLine(httpInData, 0, max_length_of_data);
			String body = new String(httpInData);
			/*      ^
		      The String variable body contains the sent (JSON) Data. 
		      Complete the implementation below.*/
			JSONObject jsonObject;
			try {
				jsonObject = new JSONObject(body);
				String author = jsonObject.getString("author");
				String text = jsonObject.getString("text");
				if (text.isEmpty()) {
					// query wasn't wrong, just the content of parameters
					resp.setStatus(HttpServletResponse.SC_ACCEPTED);
					return;
				}
				Tweet tweet = Database.insertTweet(author, text);
				JSONObject jt = new JSONObject(tweet);
				// hash tweet id and add to json
				String tweetIDHash = hashTweetID(tweet.getId().toString());
				jt.put("token", tweetIDHash);
				jt.remove("class");
				resp.getWriter().println(jt.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	private String hashTweetID(String tweetID) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		String salt = "ASW";
		String toBeHashed = tweetID + salt;
		md.update(toBeHashed.getBytes());
		byte[] digest = md.digest();
		String tweetIDHash = String.format("%032x", new BigInteger(1, digest));
		return tweetIDHash;
	}
	
	@Override
	// Implements DELETE http://localhost:8080/waslab03/tweets/:id
	public void doDelete(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		String uri = req.getRequestURI();
		try {
			long id = Long.valueOf(uri.substring(TWEETS_URI.length()));
			String key = "tweet" + id;
			// manually read parameters from body
			int max_length_of_data = req.getContentLength();
			byte[] httpInData = new byte[max_length_of_data];
			ServletInputStream  httpIn  = req.getInputStream();
			httpIn.readLine(httpInData, 0, max_length_of_data);
			String body = new String(httpInData);
			// parse body as json
			JSONObject jsonObject;
			jsonObject = new JSONObject(body);
			// compare received hash & computed hash
			String tweetIDHashReceived = jsonObject.getString(key);
			String tweetIDHashComputed = hashTweetID(id + "");
			if (tweetIDHashComputed.equals(tweetIDHashReceived)) {
				boolean success = Database.deleteTweet(id);
//				if(!success) throw new ServletException("Though correct hash provided, couldn't delete from database");
			} else { // hashes don't match
				// request was syntactically and semantically OK, but delete wasn't successful
				resp.setStatus(HttpServletResponse.SC_ACCEPTED);
				resp.getWriter().println("Incorrect hash provided. Tweet was not deleted");
				System.out.println("Someone tried to delete a tweet without the correct hash!");
			}
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}

}
